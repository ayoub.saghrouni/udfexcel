﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ExcelDna.Integration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyLibrary
{
    public class UdfFunctions
    {
        private static readonly WebClient webClient = new WebClient();

        private const string UrlTemplate = "https://jsonplaceholder.typicode.com/todos/1";

        [ExcelFunction(Description = " udf : renvoie le carre du nombe passé en parametre ")]
        public static double CalculateCarreUdf(double a)
        {
            return a * a;
        }

        [ExcelFunction(Description = " udf : renvoie une valeur from api ")]
        public static string GetDataFromApi (){
            string request = string.Format(UrlTemplate);
            string response = webClient.DownloadString(request);
            ApiResult result = new ApiResult();
            string strRemSlash = response.Replace("\"", "\'");
            string strRemNline = strRemSlash.Replace("\n", " ");
            result = JsonConvert.DeserializeObject<ApiResult>(@strRemNline);
            return result.title;
        }
    }
}
